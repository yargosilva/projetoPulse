package com.examples.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.models.email.Email;
import com.example.models.log.Log;
import com.example.models.log.services.LogEmail;

@Service
public class EmailService {
	
	@Autowired
	private LogService logService;
	
	//Estudar sobre a mudança para Observable
	
	public void enviarEmail(Email email) {
		System.out.println(email.getClass().getName());
		System.out.println("Enviando e-mail do tipo:"+email.getEmailTipo().toString());
		Log log = new LogEmail();
		logService.criarLog(log);
		System.out.println("Criando log");
	}
}
