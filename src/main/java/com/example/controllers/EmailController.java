package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.models.email.Email;
import com.examples.services.EmailService;

@RestController
@RequestMapping("/email")
public class EmailController {
	
	@Autowired
	private EmailService emailService;

	@PostMapping("/enviar")
	public ResponseEntity<String> enviarEmail(@RequestBody Email email) {
		System.out.println("Teste");
		emailService.enviarEmail(email);
		return new ResponseEntity<String>("E-mail enviado com sucesso", HttpStatus.CREATED);
	}
}
