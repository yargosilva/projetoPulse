package com.example.models.email;

import javax.validation.constraints.NotBlank;

public abstract class Email {
	
	@NotBlank
	private String nome;
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String titulo;
	
	@NotBlank
	private String mensagem;

	private EmailTipo emailTipo;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public EmailTipo getEmailTipo() {
		return emailTipo;
	}

	public void setEmailTipo(EmailTipo emailTipo) {
		this.emailTipo = emailTipo;
	}

	public abstract void enviarEmail();
}
