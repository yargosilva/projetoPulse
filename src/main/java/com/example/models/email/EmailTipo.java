package com.example.models.email;

import com.example.models.email.services.EmailExterno;
import com.example.models.email.services.EmailInterno;

public enum EmailTipo {

	INTERNO {
		@Override
		public EmailInterno getServicoEmail() {
			// TODO Auto-generated method stub
			return new EmailInterno();
		}
	},
	EXTERNO {
		@Override
		public EmailExterno getServicoEmail() {
			// TODO Auto-generated method stub
			return new EmailExterno();
		}
	};
	public abstract Email getServicoEmail();

}
